﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Text;

namespace TicTacToe
{
	class LogicGames
	{
		private readonly State[] board = new State[9];

		public int MovesCountrer { get; private set; } //счестчик шагов

		public LogicGames()
		{
			for (int i = 0; i < board.Length; i++)
			{
				board[i] = State.Unset; //инициализируем ячейки как пустое значение
			}
		}

		/// <summary>
		/// метод, который определяет кто должен ходить + не дает завершить игру раньше.
		///  [index - 1], '-1' делается для того чтобы игорк вводил более понятную ему нумерацию т.е.
		/// Как видит массив:
		/// 0 1 2
		/// 3 4 5
		/// 6 7 8
		/// Как будет ввидить игрок:
		/// 1 2 3
		/// 4 5 6
		/// 7 8 9
		/// </summary>
		/// <param name="index">параметр 'index' принимает значение, которое вводит игрок.</param>
		public void MakeMove(int index)
		{
			board[index - 1] = MovesCountrer % 2 == 0 ? State.Cross : State.Zero;

			MovesCountrer++; //+1 шаг
		}

		/// <summary>
		/// возвращаем исходное значение клетки
		/// </summary>
		/// <param name="index"></param>
		/// <returns></returns>
		public State GetState(int index)
		{
			return board[index - 1];
		}

		/// <summary>
		/// заполняем ввиде аргумента исходы побед
		/// </summary>
		/// <returns></returns>
		public Winner GetWinner()
		{
			return GetWinner(1, 4, 7, 2, 5, 8, 3, 6, 9,
				1, 2, 3, 4, 5, 6, 7, 8, 9,
				1, 5, 9, 3, 5, 7);
		}

		/// <summary>
		/// метод, который определяет победителя
		/// </summary>
		/// <param name="indexes"></param>
		/// <returns></returns>
		private Winner GetWinner(params int[] indexes)
		{
			//инкримент на 3, чтобы проверять значения переданные в параметре
			for (int i = 0; i < indexes.Length; i +=3)
			{
				//проверяется чтобы по этим индексам стоит один и тот же знак(Х,0)
				bool same = AreSame(indexes[i], indexes[i + 1], indexes[i + 2]);
				if (same)
				{
					//проверяем, что есть знак (Х,О)
					State state = GetState(indexes[i]);
					if (state != State.Unset)
					{
						//возвращаем победителя
						return state == State.Cross ? Winner.Crosses : Winner.Zeroes;
					}
				}
			}
			//игра не закончена
			if (MovesCountrer < 9)
				return Winner.GameIsUndefinished;
			//ничья
			return Winner.Draw;
		}

		/// <summary>
		/// Проверяем, что можно соедить одной линией (Х,О)
		/// </summary>
		/// <param name="a"></param>
		/// <param name="b"></param>
		/// <param name="c"></param>
		/// <returns></returns>
		private bool AreSame(int a, int b, int c)
		{
			return GetState(a) == GetState(b) && GetState(a) == GetState(c);
		}
	}
}
