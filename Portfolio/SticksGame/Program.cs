﻿using System;

namespace SticksGame
{
	class Program
	{
		static void Main(string[] args)
		{
			var game = new Sticks(10, Player.Human);
			game.MachinePlayed += Game_MachinePlayer;
			game.HumanTurnToMakeMove += Game_HumanTurnToMakeMove;
			game.EndOfGame += Game_EndOfGame;

			game.Start();
		}

		private static void Game_EndOfGame(Player player)
		{
			Console.WriteLine($"Победитель: {player}");
		}

		private static void Game_HumanTurnToMakeMove(object sender, int remainigSticks)
		{
			Console.WriteLine($"Остается палок: {remainigSticks}");
			Console.Write($"Возьмите палки ");

			bool takenCorrectly = false;

			while (!takenCorrectly)
			{
				if (int.TryParse(Console.ReadLine(), out int takenSticks))
				{
					var game = (Sticks) sender;

					try
					{
						game.HumanTakes(takenSticks);
						takenCorrectly = true;
					}
					catch(AggregateException ex)
					{
						Console.WriteLine(ex.Message);
					}
				}
			}
		}

		private static void Game_MachinePlayer(int sticksTaken)
		{
			Console.WriteLine($"Машина взяла {sticksTaken}");
			Console.WriteLine("____________________________");
		}
	}
}
