﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Design;
using System.Text;

namespace SticksGame
{
	class Sticks
	{
		private readonly Random randomizer;
		public int InitialStickNumber { get; }
		public Player Turn { get; private set; }
		public int RemainingSticks { get; private set; }
		public GameStatus GameStatus { get; private set; }

		public event Action<int> MachinePlayed;
		public event EventHandler<int> HumanTurnToMakeMove;

		public event Action<Player> EndOfGame;

		public Sticks(int initialStickNumber, Player whoMakesFirstmove)
		{
			if (initialStickNumber < 7 || initialStickNumber > 30)
				throw new ArgumentException("Первоначальное количество палочек должно быть >= 7 и <= 30");

			randomizer = new Random();
			GameStatus = GameStatus.NotStarted;
			InitialStickNumber = initialStickNumber;
			RemainingSticks = InitialStickNumber;
			Turn = whoMakesFirstmove;
		}

		public void Start()
		{
			if (GameStatus == GameStatus.GameIsOver)
			{
				RemainingSticks = InitialStickNumber;
			}

			if (GameStatus == GameStatus.InProgress)
			{
				throw new InvalidOperationException("Невозможно вызвать Start, когда игра уже выполняется.");
			}

			GameStatus = GameStatus.InProgress;
			while (GameStatus == GameStatus.InProgress)
			{
				if (Turn == Player.Computer)
				{
					ComMakesMove();
				}
				else
				{
					HumanMakesMove();
				}

				FireEndOfGameIfRequired();

				Turn = Turn == Player.Computer ? Player.Human : Player.Computer;
			}	
		}

		private void FireEndOfGameIfRequired()
		{
			if (RemainingSticks == 0)
			{
				GameStatus = GameStatus.GameIsOver;
				if (EndOfGame != null)
					EndOfGame(Turn == Player.Computer ? Player.Human : Player.Computer);
			}
		}

		private void HumanMakesMove()
		{
			if (HumanTurnToMakeMove != null)
				HumanTurnToMakeMove(this, RemainingSticks);
		}

		private void ComMakesMove()
		{
			int maxNumber = RemainingSticks >= 3 ? 3 : RemainingSticks;

			int sticks = randomizer.Next(1, maxNumber);

			TakeSticks(sticks);
			if (MachinePlayed != null)
				MachinePlayed(sticks);
		}

		private void TakeSticks(int sticks)
		{
			RemainingSticks -= sticks;
		}

		public void HumanTakes(int sticks)
		{
			if (sticks < 1 || sticks > 3)
				throw new ArgumentException("Вы можете взять от 1 до 3 палочек за один ход.");

			if (sticks > RemainingSticks)
				throw new ArgumentException("Вы не можете взять больше, чем осталось. Остается: {RemainingSticks}");

			TakeSticks(sticks);
		}
	}
}
