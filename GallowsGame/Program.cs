﻿using System;

namespace GallowsGame
{
	class Program
	{
		static void Main(string[] args)
		{
			LogicsGame logicsGame = new LogicsGame();

			string word = logicsGame.GenerateWord();

			Console.WriteLine($"Загаданное слово состоит из {word.Length} букв");
			Console.WriteLine("Попробуй угадать слово");

			while (logicsGame.GameStatus == GameStatus.InProgress)
			{
				Console.Write("Буква: ");
				char c = Console.ReadLine().ToCharArray()[0];

				string curState = logicsGame.GetLetter(c); //текущий статус
				Console.WriteLine(curState);

				Console.WriteLine($"Осталось попыток: {logicsGame.RemainingTries}"); //осталось попыток
				Console.WriteLine($"Вы использовали следующие буквы: {logicsGame.TriedLetters}"); //список букв, использованных

				if (logicsGame.GameStatus == GameStatus.Lost)
				{
					Console.WriteLine("Вы проиграли");
					Console.WriteLine($"загаданное слово было \"{logicsGame.Word}\"");
				}
				else if(logicsGame.GameStatus == GameStatus.Win)
				{
					Console.WriteLine("Поздравляю, Вы выйграли");
				}
			}
		}
	}
}